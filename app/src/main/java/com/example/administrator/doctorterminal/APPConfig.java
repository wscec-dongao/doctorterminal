package com.example.administrator.doctorterminal;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2021/3/24.
 */
public class APPConfig {
    static String DEVICEID = "deviceId";
    static String  VERSION = "version";

    private static APPConfig instance = null;
    static SharedPreferences settings ;

    private APPConfig() {
    }

    public static APPConfig getInstance()
    {
        if(instance == null)
        {
            instance = new APPConfig();
        }
        return instance;
    }

    public void init(Context context)
    {
         settings = context.getSharedPreferences(this.getClass().getName(), Context.MODE_PRIVATE);
         APPConfig.getInstance().writeConfig(VERSION,"v1");
    }
    public static String readConfig(String key,String defaultValue)
    {
        String value = settings.getString(key, defaultValue);
        return value;
    }

    public static String writeConfig(String key,String value)
    {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key,value);
        editor.commit();
        return value;
    }


}
