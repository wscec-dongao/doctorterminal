package com.example.administrator.doctorterminal;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cai on 2019/7/26.
 */
public class Utils {
    //将 WRITE_FLAG SHOW_FLAG 作为调试开关
    public static int WRITE_FLAG = 1;
    public static int SHOW_FLAG = 1;
    public static int SHOW = 1;
    static Map<String, PrintStream> writeFlie = new HashMap<String, PrintStream>();
    static TextView text = null;
    static TextView defaultText = null;
    public static void  initNotice(Context context)
    {
        text = new TextView(context);
        defaultText = new TextView(context);
    }
    public static  enum NoticGrade{
        NULL, WARNING, NORMAL

    }
//    public static void notice(final Context context, final String status) {
//
//        ((Activity) context).runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (SHOW_FLAG == 1) {
//                    Toast toast = Toast.makeText(context, status, Toast.LENGTH_SHORT);
//                    LinearLayout.LayoutParams textParam = new LinearLayout.LayoutParams
//                            (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
//                    defaultText.setText(status);	// 设置文本内容
//                    defaultText.setTextColor(context.getResources().getColor(R.color.white));	// 文本颜色
//                    defaultText.setTextSize(18);	// 文本字体大小
//                    defaultText.setLayoutParams(textParam);
//                    defaultText.setPadding(80, 40, 80, 40);
//                    defaultText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);	// 设置文本居中
//                    defaultText.setBackgroundColor(context.getResources().getColor(R.color.softBlue));	// 设置背景颜色
//                    GradientDrawable gd = new GradientDrawable();//创建drawable
//                    int strokeWidth = 0; // 3dp 边框宽度
//                    int roundRadius = 90; // 8dp 圆角半径
//                    int strokeColor = Color.parseColor("#2E3135");//边框颜色
//                    int fillColor = Color.parseColor(String.format("#%x", context.getResources().getColor(R.color.softBlue)));//内部填充颜色
//                    gd.setColor(fillColor);
//                    gd.setAlpha(230);
//                    gd.setCornerRadius(roundRadius);
//                    gd.setStroke(strokeWidth, strokeColor);
//                    //修改 notice 位置
//                    Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
//                    // 获取屏幕高度
//                    int height = display.getHeight();
//                    // 这里给了一个1/4屏幕高度的y轴偏移量
//                    toast.setGravity(Gravity.TOP, 0, height / 4);
//
//                    defaultText.setBackground(gd);
//                    toast.setView(defaultText); // 将文本插入到toast里
//                    toast.show();
//                }
//
//            }
//        });
//    }
//    public static void notice(final Context context, final String status, final  NoticGrade noticGrade) {
//
//        ((Activity) context).runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (SHOW_FLAG == 1) {
//                    Drawable dra;
//                    int strokeWidth = 0;
//                    int roundRadius = 0;
//                    int strokeColor = 0;
//                    int fillColor = 0;
//
//                    Toast toast = Toast.makeText(context, status, Toast.LENGTH_SHORT);
//                    LinearLayout.LayoutParams textParam = new LinearLayout.LayoutParams
//                            (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
//                    text.setText(status);	// 设置文本内容
//                    text.setTextColor(context.getResources().getColor(R.color.white));	// 文本颜色
//                    text.setTextSize(16);	// 文本字体大小
//                    text.setLayoutParams(textParam);
//                    text.setPadding(80, 40, 80, 40);
//                    text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);	// 设置文本居中
//                    text.setBackgroundColor(context.getResources().getColor(R.color.softBlue));	// 设置背景颜色
//                    GradientDrawable gd = new GradientDrawable();//创建drawable
//                     strokeWidth = 0; // 3dp 边框宽度
//                     roundRadius = 90; // 8dp 圆角半径
//                     strokeColor = Color.parseColor("#2E3135");//边框颜色
//
//                    switch(noticGrade)
//                    {
//                        case WARNING:
//                            dra= context.getResources().getDrawable(R.drawable.notice_warnning);
//                            dra.setBounds(0, 0, dra.getIntrinsicWidth(), dra.getIntrinsicHeight());
//                            text.setCompoundDrawables(dra,null,null,null);
//                            text.setCompoundDrawablePadding(20);
//                            fillColor = Color.parseColor(String.format("#%x", context.getResources().getColor(R.color.dark_red)));//内部填充颜色
//                        break;
//                        case NORMAL:
//                             dra= context.getResources().getDrawable(R.drawable.notice_normal);
//                            dra.setBounds(0, 0, dra.getIntrinsicWidth(), dra.getIntrinsicHeight());
//                            text.setCompoundDrawables(dra,null,null,null);
//                            text.setCompoundDrawablePadding(20);
//                            fillColor = Color.parseColor(String.format("#%x", context.getResources().getColor(R.color.softBlue)));//内部填充颜色
//                        break;
//                        case NULL:
//                            text.setCompoundDrawables(null, null, null, null);
//                            fillColor = Color.parseColor(String.format("#%x", context.getResources().getColor(R.color.softBlue)));//内部填充颜色
//                            break;
//                        default:
//                            break;
//
//                    }
//                    //修改 notice 位置
//                    Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
//                    // 获取屏幕高度
//                    int height = display.getHeight();
//                    // 这里给了一个1/4屏幕高度的y轴偏移量
//                    toast.setGravity(Gravity.TOP, 0, height / 4);
//
//                    gd.setColor(fillColor);
//                    gd.setAlpha(250);
//                    gd.setCornerRadius(roundRadius);
//                    gd.setStroke(strokeWidth, strokeColor);
//                    text.setBackground(gd);
//                    toast.setView(text); // 将文本插入到toast里
//                    toast.show();
//                }
//
//            }
//        });
//    }
    //作为用户提醒
    public static void notice(final Context context, final String status, final int flag) {

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (flag == SHOW) {
                    Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public static PrintStream registerDebugFile(Context context, String fileName) {
        String path = fileName;
        PrintStream printStream = null;
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString(), path);
        try {
            printStream = new PrintStream(new FileOutputStream(file, true), true);
            writeFlie.put(fileName, printStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return printStream;

    }

    public static void Write(String fileName, String data) {
        if (WRITE_FLAG == 1) {
            Long timeStamp = System.currentTimeMillis();  //获取当前时间戳
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sd = sdf.format(new Date(Long.parseLong(String.valueOf(timeStamp))));      // 时间戳转换成时间
            if(writeFlie.get(fileName) != null)
            {
                writeFlie.get(fileName).println("***"+ sd + ": " + data);
                writeFlie.get(fileName).flush();

            }
        }

    }
    public static void WriteWithoutTimestamp(String fileName, String data) {
        if (WRITE_FLAG == 1) {
            if(writeFlie.get(fileName) != null)
            {
                writeFlie.get(fileName).println(data);
                writeFlie.get(fileName).flush();

            }
        }

    }
    public  static void closeDebugFile()
    {
        for(int i = 0; i < writeFlie.size(); i++)
        {
            //关闭所有注册的文件

        }

    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dp(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

}
