package com.example.administrator.doctorterminal;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2021/1/7.
 */

public class GetLocInfo {
    private final static String TAG = "GetLocInfo";
    private final LocationManager locationManager;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Criteria mCriteria = new Criteria();
    Context context;
    private boolean bLocationEnable = false;
    private String mLocation;
    private LocInfo myLocInfo ;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式String date = df.format(new Date());
    JSONObject LatLon = new JSONObject();
    public interface  LocInfo
    {
        void toastLocInfo(String locInfo);

    }

    public GetLocInfo(Context context) {
        this.context = context;
        myLocInfo = (LocInfo) context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // 设置定位精确度 Criteria.ACCURACY_COARSE 比较粗略， Criteria.ACCURACY_FINE 则比较精细
        mCriteria.setAccuracy(Criteria.ACCURACY_FINE);
        // 设置是否需要海拔信息 Altitude
        mCriteria.setAltitudeRequired(false);
        // 设置是否需要方位信息 Bearing
        mCriteria.setBearingRequired(false);
        // 设置是否允许运营商收费
        mCriteria.setCostAllowed(true);
        // 设置对电源的需求
//        mCriteria.setPowerRequirement(Criteria.POWER_LOW);
        Log.d(TAG, "initLocation");
        initLocation();
        mHandler.postDelayed(mRefresh, 1000);
//        locationManager.addGpsStatusListener(statusListener)
    }
    private void initLocation() {
        String bestProvider = locationManager.getBestProvider(mCriteria, true);
        if (bestProvider == null) {
         bestProvider = LocationManager.NETWORK_PROVIDER;
        }
        if (locationManager.isProviderEnabled(bestProvider)) {
//            tv_location.setText("正在获取"+bestProvider+"定位对象");
            mLocation = String.format("\n定位类型=%s", bestProvider);
            beginLocation(bestProvider);
            bLocationEnable = true;
        } else {
//          tv_location.setText("\n"+bestProvider+"定位不可用");
            Log.d(TAG, "\n" + bestProvider + "定位不可用");
            bLocationEnable = false;
        }
    }
    private void setLocationText(Location location) {
        if (location != null) {
            String desc = String.format("%s\n定位对象信息如下： " +
                            "\n\t其中时间：%s" +
                            "\n\t其中经度：%f，纬度：%f" +
                            "\n\t其中高度：%d米，精度：%d米",
                    mLocation, Util.getNowDateTimeFormat(),
                    location.getLongitude(), location.getLatitude(),
                    Math.round(location.getAltitude()), Math.round(location.getAccuracy()));
//            Log.d(TAG, desc);

            String date = df.format(new Date());
            try {
                LatLon.put("deviceId",APPConfig.readConfig(APPConfig.DEVICEID,"d0001"));
                LatLon.put("lat",String.format("%.6f",location.getLatitude()));
                LatLon.put("lon",String.format("%.6f",location.getLongitude()));
                LatLon.put("currentStamp",date);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            myLocInfo.toastLocInfo(LatLon.toString());
//            tv_location.setText(desc);
        } else {
//            myLocInfo.toastLocInfo(mLocation+" 暂未获取到定位对象");
//            tv_location.setText(mLocation+"\n暂未获取到定位对象");
        }
    }

    private void beginLocation(String method) {
//        myLocInfo.toastLocInfo("使能\n");
        locationManager.requestLocationUpdates(method, 1000, 0, mLocationListener);
        Location location = locationManager.getLastKnownLocation(method);
        setLocationText(location);
    }

    // 位置监听器
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            setLocationText(location);
        }

        @Override
        public void onProviderDisabled(String arg0) {
        }

        @Override
        public void onProviderEnabled(String arg0) {
        }

        @Override
        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

        }
    };
    private boolean isGPSEnable() {

        String str = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        Log.v("GPS", str);

        if (str != null) {

            return str.contains("gps");

        } else{

            return false;

        }        }

    private Runnable mRefresh = new Runnable() {
        @Override
        public void run() {

            if (bLocationEnable == false) {
                initLocation();
                mHandler.postDelayed(this, 1000);
            }

        }
    };
    public  void onDestroy() {
        if (locationManager != null) {
            locationManager.removeUpdates(mLocationListener);
        }

    }
    public static void TEST_GetLocInfo(Context context)
    {
        GetLocInfo LocInfo = new GetLocInfo(context);




    }



}
