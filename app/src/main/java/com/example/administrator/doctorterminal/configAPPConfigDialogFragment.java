package com.example.administrator.doctorterminal;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.ToggleButton;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link configAPPConfigDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link configAPPConfigDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class configAPPConfigDialogFragment extends DialogFragment implements DialogInterface.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View diaglogView;
    Spinner spinner;
    ToggleButton enable_local_eph_data;

    private OnFragmentInteractionListener mListener;
    private AlertDialog configDiaglog;
    private Uri.Builder  uriBuilder;

    private View select_eph_data_layout;
    private LinearLayout select_eph_data_box;
    private Spinner select_eph_data_spinner;
    private DatePicker setDate;
    private TimePicker setTime;
    private boolean calling;
    private ToggleButton enable_simulate_time;
    private View config_simulate_time_layout;
    private LinearLayout config_simulate_time_box;
    private EditText deviceId;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment configSimaluteSourceDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static configAPPConfigDialogFragment newInstance(String param1, String param2) {
        configAPPConfigDialogFragment fragment = new configAPPConfigDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public configAPPConfigDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        uriBuilder =new Uri.Builder() ;
        uriBuilder.scheme("configSimaluteSourceDialogFragment");
        uriBuilder.authority("configSimaluteSource");
        uriBuilder.path("config");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.fragment_configapp_dialog, null);

        deviceId = (EditText) v.findViewById(R.id.inputDeviceId);
        configDiaglog = new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(R.string.config)
                .setPositiveButton(android.R.string.ok, this)
                .create();

        return configDiaglog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        diaglogView =  inflater.inflate(R.layout.fragment_configapp_dialog, container, false);
        // Inflate the layout for this fragment
        return diaglogView;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        switch (which) {
            case DialogInterface.BUTTON_NEGATIVE:
                // int which = -2
                dialogInterface.dismiss();
                break;
            case DialogInterface.BUTTON_NEUTRAL:
                // int which = -3
                dialogInterface.dismiss();
                break;
            case DialogInterface.BUTTON_POSITIVE:
                // int which = -1
                APPConfig.writeConfig(APPConfig.DEVICEID,"d"+deviceId.getText().toString());
                dialogInterface.dismiss();

                break;
        }
        setCalling(false);
    }



    //防止出现二次调用
    @Override
    public void show(FragmentManager manager, String tag)
    {
       if(!isCalling())
        {
            super.show(manager,tag);
            setCalling(true);
        }



    }

    public boolean isCalling() {
        return calling;
    }

    public void setCalling(boolean calling) {
        this.calling = calling;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
